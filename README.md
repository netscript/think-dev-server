简单开启 开发测试服务器
_________________
# think-dev-server

> start server [启动服务器] 

    php think dev
    
    # default listen host 127.0.0.1 at port 80
    # 默认监听 本地 80端口
    # default document root at porject public dir
    # 默认服务器 工作路径在 项目的 public 目录
    # default router script at public dir which name router.php
    # 默认指定 路由处理脚本 为 项目public目录下的 router.php 
   
> command options [命令可选参数]

    php think dev -h # print man read command optins args
                     # 输出命令 提示 参数 信息     
    
    -p=:port
    --port=:port  # support developer input port args
                  # 支持 自定义监听端口 如果端口已被占用 命令会提示
    
    -d=:dir
    --dir=:dir    # support developer choose document root
                  # 支持 指定 服务器工作目录
                  
    -i=:ip
    -ip=:ip       # support ip choose bind
                  # 支出 指定 服务器 绑定对应(本机网卡)ip地址              
    
    -r=:router
    --route=:router  # support route sciprt choose to handle
                     # 支出 指定 处理 路由脚本