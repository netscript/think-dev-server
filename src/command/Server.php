<?php
/**
 * Created by Hw.
 * User: Weblinuxgame
 * Date: 2018-08-17
 * Time: 14:45
 */

namespace think\weblinuxgame\command;


use think\console\Input;
use think\console\Output;
use think\console\Command;
use think\console\input\Option;

/**
 * 开启 内置 测试服务器
 * Class Server
 * @package app\common\command
 */
class Server extends Command
{
    const DEFAULT_PORT = 80;
    const DEFAULT_ROUTE_FILE_NAME = 'router.php';
    const EXEC_COMMAND = 'php -S ${host}:${port} -t ${dir} ${router}';
    const DEFAULT_HOST = '0.0.0.0';
    protected static $replace = ['${host}', '${port}', '${dir}', '${router}'];


    /**
     * 配置命令
     */
    protected function configure()
    {
        $help = 'Test server command options %s' .
            '  --port,-p server listen port ' . PHP_EOL .
            '  --dir,-d server work document root dirname ' . PHP_EOL .
            '  --ip,-i  server bind host ip ' . PHP_EOL .
            '  --route,-r server route handler script filename ' . PHP_EOL
            . ' eg: php think dev -d=./public -p=8080 -r=./public/router.php ' . PHP_EOL;
        $help = sprintf($help, PHP_EOL);
        $this->setName('server')->setDescription('developer use for builtin http server');
        $this->setAliases(['dev']);
        $this->setHelp($help);
        $this->addOption('--port', '-p', Option::VALUE_OPTIONAL, 'set test server listen port');
        $this->addOption('--dir', '-d', Option::VALUE_OPTIONAL, 'set test server document root');
        $this->addOption('--route', '-r', Option::VALUE_OPTIONAL, 'set test server router');
        $this->addOption('--ip', '-i', Option::VALUE_OPTIONAL, 'set test server listen host ip');
    }

    /**
     * 运行 命名解析
     * @param Input $input
     * @param Output $output
     * @return bool|int|string
     */
    public function run(Input $input, Output $output)
    {
        $script = realpath($_SERVER['SCRIPT_NAME']);
        $dir = $input->getParameterOption('--dir') ?: $input->getParameterOption('-d', $script);
        $port = $input->getParameterOption('--port') ?: $input->getParameterOption('-p', self::DEFAULT_PORT);
        $router = $input->getParameterOption('--route') ?: $input->getParameterOption('-r', '');
        $host = $input->getParameterOption('--ip') ?: $input->getParameterOption('-i', self::DEFAULT_HOST);
        $command = self::EXEC_COMMAND;
        if (!is_numeric($port)) {
            $port = self::DEFAULT_PORT;
        }
        if (empty($router)) {
            $router = $dir . DS . self::DEFAULT_ROUTE_FILE_NAME;
        }
        if (!is_dir($dir)) {
            $dir = dirname(APP_PATH) . DS . 'public';
        }
        if (!file_exists($dir) && file_exists($script)) {
            $dir = dirname($script);
        }
        if (!empty($router) && !file_exists($router)) {
            $router = $dir . DS . self::DEFAULT_ROUTE_FILE_NAME;
        }
        if (!file_exists($router)) {
            $router = null;
        }
        $args = ['host' => $host, 'port' => $port, 'router' => $router, 'dir' => $dir];
        foreach ($args as $key => $val) {
            if ($key && in_array('${' . $key . '}', self::$replace)) {
                $command = str_replace('${' . $key . '}', is_null($val) ? '' : $val, $command);
            }
        }
        $tip = '#################################### %s' .
            'dev server is running at host ' . PHP_EOL .
            $host . ' listen port : %s ' . PHP_EOL .
            'Document root at ' . realpath($dir) . PHP_EOL .
            '#####################################' . PHP_EOL;
        $err_no = '';
        $err_str = '';
        $isEnable = self::checkPortEnable($host, $port, $err_no, $err_str);
        if (!$isEnable) {
            $output->write(sprintf('bind host : %s listen port: %s failed', $host, $port));
            $output->write($err_str);
            return $err_no;
        }
        $tip = sprintf($tip, PHP_EOL, $port);
        return $this->exec($command, $tip, $output);
    }

    /**
     * 命令执行
     * @param $command
     * @param string $tip
     * @param Output $output
     * @return bool|string
     */
    protected function exec($command, $tip = PHP_EOL, Output $output)
    {
        if (function_exists('system')) {
            $output->write($tip);
            return system($command);
        }
        return 0;
    }

    /**
     * 检查 端口是否被占用
     * @param $host
     * @param $port
     * @param null $err_no
     * @param null $err_str
     * @return bool
     */
    protected static function checkPortEnable($host, $port, &$err_no = null, &$err_str = null)
    {
        $socket = stream_socket_server("tcp://$host:$port", $err_no, $err_str);
        if (self::DEFAULT_HOST === $host) {
            $host = '127.0.0.1';
        }
        // 不为空 能绑定 可用
        $enable = empty($socket) ? false : true;
        fclose($socket);
        unset($socket);
        if ($enable) {
            return @file_get_contents("http://$host:$port/index.php",
                false,
                stream_context_create(['http' => ['method' => "GET", 'timeout' => 3]])
            ) ? false : true;
        }
        return $enable;
    }
}